import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'dowar-blog',
  webDir: 'dist',
  bundledWebRuntime: false
  // server: {
  //   url: "http://192.168.1.68:8100",
  //   cleartext: true,
  // },
};

export default config;
