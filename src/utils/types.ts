export type Theme = "light" | "nord" | "gruvbox";

export type EmailFilterOption = "all" | "verified" | "unverified";
export type UserFilterOption = "all" | "enabled" | "disabled";
export type UserRolFilterOption = "all" | "admin";
export type DesignMode = "md" | "ios";
